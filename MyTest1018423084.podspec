Pod::Spec.new do |s|

s.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  s.name         = "MyTest1018423084"
  s.version      = "0.0.39"
  s.summary      = "Mi primera prueba"
  s.description  = <<-DESC
  una descripcion cualquiera
                   DESC

  s.homepage     = "mipagina.com"
  s.license      =  "Copyright @Felipe"

  s.author             = { "David Felipe Cortes" => "davidfelipe.89@hotmail.com" }
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'TransunionSDK.framework'
  s.source            = { :http => 'https://gitlab.com/Felo2602/transunionsdkpods/-/raw/a335d59d72d965f8e3b599c92b0a8ec28d148273/TransunionSDK.framework.zip' }
  s.exclude_files = "Classes/Exclude"

end
